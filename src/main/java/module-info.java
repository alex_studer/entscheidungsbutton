module ch.zhaw.studer.le03a_entscheidungsknopf {
    requires javafx.controls;
    requires javafx.fxml;


    opens ch.zhaw.studer.le03a_entscheidungsknopf to javafx.fxml;
    exports ch.zhaw.studer.le03a_entscheidungsknopf;
}
