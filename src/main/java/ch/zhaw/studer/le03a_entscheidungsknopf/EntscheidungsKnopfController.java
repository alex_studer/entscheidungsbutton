package ch.zhaw.studer.le03a_entscheidungsknopf;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class EntscheidungsKnopfController {

    @FXML
    private Label inputAktivitaetText;

    @FXML
    private Button entscheidungsButton;

    @FXML
    private GridPane gridPane;

    @FXML
    private Button saveButton;

    @FXML
    TextField input;

    @FXML
    Label errorMsg;
    @FXML
    private Label filler;
    private List<String> aktivitaetenListe = new ArrayList<>();
    private boolean isQuestionAsked = false;

    static final String ERROR_MSG_DUPLICAT = "Aktivitaet existiert schon in der Aktivitaeten Liste.";
    static final String ERROR_MSG_LEER = "Bitte eine Aktivitaet eingeben.";

    private static final String FONT_SIZE = "-fx-font-size: 13";


    private Random random = new Random();
    private IntStream intStream;

    public EntscheidungsKnopfController() {
        aktivitaetenListe.add("Ins Kino.");
        aktivitaetenListe.add("Essen gehen.");
        aktivitaetenListe.add("Spazieren gehen.");
        aktivitaetenListe.add("Bowlen.");
        aktivitaetenListe.add("Joggen.");
        aktivitaetenListe.add("Ins Theater.");
        aktivitaetenListe.add("In die Oper.");
        aktivitaetenListe.add("An einen Fussball match.");
        aktivitaetenListe.add("An ein Konzert.");

    }

    @FXML
    protected void onButtonClick() {
        errorMsg.setText("");

        if (isQuestionAsked) {
            entscheidungsButton.textProperty().set(getRandomText());
            getInputAktivitaet();
            input.setText("");
            //input.visibleProperty().set(true);
        } else {
            input.textProperty().set("");
            inputAktivitaetText.setText("Was unternehmen wir heute Abend?");
            entscheidungsButton.textProperty().set("Click me!");
            isQuestionAsked = true;
        }

    }

    @FXML
    protected void onSaveButtonClick() {

        String validateErrorMsg = validInputAktivitaet(input.getText());
        if (validateErrorMsg.isEmpty()) {
            errorMsg.setText(input.getText() + " wurde zu den bestehenden Aktivitäten abgespeichert.");
            entscheidungsButton.textProperty().set(input.getText());
            aktivitaetenListe.add(input.getText());
            input.setText("");

        } else {
            errorMsg.setText(validateErrorMsg);
        }


    }

    private String validInputAktivitaet(String neueAktivitaet) {
        if (aktivitaetenListe.contains(neueAktivitaet)) {
            return ERROR_MSG_DUPLICAT;
        }
        if (neueAktivitaet.isEmpty()){
            return ERROR_MSG_LEER;
        }
        return "";
    }

    private void getInputAktivitaet() {

        // gridPane.gridLinesVisibleProperty().set(true);

        inputAktivitaetText.setText("Eingabe :");
        inputAktivitaetText.setStyle(FONT_SIZE);
        inputAktivitaetText.setVisible(true);

        input.setVisible(true);
        input.setMinWidth(300);
        input.setStyle(FONT_SIZE);

        saveButton.visibleProperty().set(true);
        saveButton.visibleProperty().set(true);
        saveButton.setStyle(FONT_SIZE);

        filler.setText("");
        errorMsg.setStyle(FONT_SIZE);

        gridPane.setPadding(new Insets(10, 10, 10, 10));
        GridPane.setConstraints(inputAktivitaetText, 1, 0);
        GridPane.setConstraints(input, 2, 0);
        //GridPane.setConstraints(saveButton, 3, 0);
        GridPane.setConstraints(errorMsg, 1, 3);
        GridPane.setConstraints(filler, 1, 2);
        GridPane.setColumnSpan(errorMsg, 3);

        // gridPane.getChildren().addAll(inputActionText,input,saveButton);
    }

    private String getRandomText() {
        return aktivitaetenListe.get(random.ints(1, 0, aktivitaetenListe.size()).findFirst().getAsInt());
    }

    public Label getInputAktivitaetText() {
        return inputAktivitaetText;
    }

    public void setInputAktivitaetText(Label welcomeText) {
        this.inputAktivitaetText = welcomeText;
    }

    public List<String> getAktivitaetenListe() {
        return aktivitaetenListe;
    }

    public void setAktivitaetenListe(List<String> aktivitaetenListe) {
        this.aktivitaetenListe = aktivitaetenListe;
    }
}
